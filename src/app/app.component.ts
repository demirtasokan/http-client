import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements  OnInit {
  loadedFeature = 'recipe';

  ngOnInit() {
    const config = {
      apiKey: 'AIzaSyAJfSnkAEhvlYky1WCj2Maz5JPR1L-diQM',
      authDomain: 'ng-recipe-book-b3ab4.firebaseapp.com',
      databaseURL: 'https://ng-recipe-book-b3ab4.firebaseio.com',
      projectId: 'ng-recipe-book-b3ab4',
      storageBucket: 'ng-recipe-book-b3ab4.appspot.com',
      messagingSenderId: '916882587106'
    };
    firebase.initializeApp(config);
  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }
}
